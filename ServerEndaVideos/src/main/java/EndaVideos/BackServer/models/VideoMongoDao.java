package EndaVideos.BackServer.models;

import EndaVideos.BackServer.entities.Video;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface VideoMongoDao extends MongoRepository<Video, Integer> {

   List<Video> findAllByTitleOrUserNick(String searchVideo, String searchUser);
}
