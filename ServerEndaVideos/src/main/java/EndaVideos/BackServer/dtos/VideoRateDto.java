package EndaVideos.BackServer.dtos;

public class VideoRateDto {

    private int idVideo;

    private int idUser;

    private boolean useful;

    public VideoRateDto(){}

    public VideoRateDto(int idVideo, int idUser, boolean useful) {
        this.idVideo = idVideo;
        this.idUser = idUser;
        this.useful = useful;
    }

    public int getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(int idVideo) {
        this.idVideo = idVideo;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public boolean isUseful() {
        return useful;
    }

    public void setUseful(boolean useful) {
        this.useful = useful;
    }
}
