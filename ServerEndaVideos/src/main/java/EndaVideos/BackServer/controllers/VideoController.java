package EndaVideos.BackServer.controllers;

import EndaVideos.BackServer.dtos.VideoDto;
import EndaVideos.BackServer.endaVideosProperties.VideoProperties;
import EndaVideos.BackServer.entities.User;
import EndaVideos.BackServer.entities.Video;
import EndaVideos.BackServer.models.SequenceMongoDao;
import EndaVideos.BackServer.models.UserDao;
import EndaVideos.BackServer.models.VideoMongoDao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class VideoController {

    private VideoDto videoDto;
    private VideoMongoDao videoMongoDao;
    private SequenceMongoDao sequenceMongoDao;
    private UserDao userDao;

    public VideoController(VideoDto videoDto, VideoMongoDao videoMongoDao, SequenceMongoDao sequenceMongoDao, UserDao userDao) {
        this.videoDto = videoDto;
        this.videoMongoDao = videoMongoDao;
        this.sequenceMongoDao = sequenceMongoDao;
        this.userDao = userDao;
    }

    public VideoController(VideoMongoDao videoMongoDao){
        this.videoMongoDao = videoMongoDao;
    }

    public Video createVideo(){
        Video video = new Video();
        if(validateUser(videoDto.getUserNick())){
            video = setVideoInformation();
            return video;
        }
        video.setMessage("User not found, maybe you are not register");
        video.setStatus(404);
        return video;
    }

    private boolean validateUser(String userNick) {
        User user = userDao.findByNick(userNick);
        return user != null;
    }

    private String setUrlVideo(String userNick, int videoId){
        String url = userNick + "/" + videoId;
        return url;
    }

    private Video setVideoInformation(){
        Video video = new Video();
        if(videoDto.getTitle()!=null){
            video.setId(new SequenceController().getNextSequence(sequenceMongoDao));
            video.setTitle(videoDto.getTitle());
            video.setUserNick(videoDto.getUserNick());
            video.setUseful(0);
            video.setNotUseful(0);
            video.setUrl(setUrlVideo(videoDto.getUserNick(), video.getId()));
            video.setDescription(videoDto.getDescription());
            video.setPublishDate(setServerDate());
            videoMongoDao.save(video);
            video.setStatus(200);
            video.setMessage("Video successfully created");
            return video;
        }
        video.setStatus(422);
        video.setMessage("Video sent without a title");
        return video;
    }

    private Date setServerDate(){
        Date serverDate;
        Calendar date = Calendar.getInstance();
        serverDate = date.getTime();
        return serverDate;
    }

    public List<Video> getVideoConcidences(String searchVideo){
        List<Video> videoConcidences = new ArrayList<>();
        List<Video> dataBaseVideos = videoMongoDao.findAll();
        for(int i = 0; i < dataBaseVideos.size(); i++){
            Video videoResult = dataBaseVideos.get(i);
            if(videoResult.getTitle().equalsIgnoreCase(searchVideo)){
                videoConcidences.add(videoResult);
            }
        }
        return videoConcidences;
    }

    public List<Video> getFirstVideos(){
        int quantity = VideoProperties.QUANTITY_OF_VIDEOS_FOR_HOME_PAGE;
        List<Video> videosResult;
        List<Video> videosDataBase = videoMongoDao.findAll();
        if(videosDataBase.size()>=quantity){
            videosResult = getPopularVideos(quantity, videosDataBase);
            return videosResult;
        }
        videosResult = getPopularVideos(videosDataBase.size(), videosDataBase);
        return videosResult;
    }

    private List<Video> getPopularVideos(int quantity, List<Video> videosDataBase){
        int popularity = VideoProperties.POPULARITY;
        List<Video> videosResult = new ArrayList<>();
        for(int i = 0; i < quantity; i++){
            Video videoDataBase = videosDataBase.get(i);
            if(videoDataBase.getUseful()>= popularity){
                videosResult.add(videosDataBase.get(i));
            }
        }
        return videosResult;
    }


}
