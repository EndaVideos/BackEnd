package net.serenitybdd.endavideos.glue;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.apis.EndaVideosPetitionsResources;
import net.serenitybdd.apis.PostVideoResponse;
import net.serenitybdd.apis.EndaVideosURLs;


import static net.serenitybdd.rest.SerenityRest.given;
import static net.serenitybdd.rest.SerenityRest.then;
public class PostVideoSteps {
    private RequestSpecification request;
    @Given("^the user wants to post a new video on his platform$")
    public void theUserWantsToPostANewVideoOnHisPlatform() throws Throwable {
        request = given().contentType("application/json");
    }

    @And("^the user loads the video with the necessary and valid information$")
    public void theUserLoadsTheVideoWithTheNecessaryAndValidInformation() throws Throwable {
        request.body(EndaVideosPetitionsResources.fillVideoDataPostVideo().toString());
    }

    @When("^the user select the post option$")
    public void theUserSelectThePostOption() throws Throwable {
       request.when().post(EndaVideosURLs.postVideoUrl());
    }

    @Then("^the platform should displayed the new video with the sent information$")
    public void thePlatformShouldDisplayedTheNewVideoWithTheSentInformation() throws Throwable {
        PostVideoResponse.setResponse(then().extract().response()).validatePostSuccessResponse(EndaVideosPetitionsResources.getRequestParams());

    }


    @And("^the user loads the video without the title$")
    public void theUserLoadsTheVideoWithoutTheTitle() throws Throwable {
        request.body(EndaVideosPetitionsResources.fillVideoDataPostVideoWithOutTitle().toString());
    }


    @Then("^the platform display a message indicating \"([^\"]*)\"$")
    public void thePlatformDisplayAMessageIndicating(String informationMessage) throws Throwable {
        PostVideoResponse.setResponse(then().extract().response()).validatePostVideoWithoutTitle(informationMessage);

    }

    @Given("^an user that is not registered in the platform$")
    public void anUserThatIsNotRegisteredInThePlatform() throws Throwable {
        request = given().contentType("application/json").body(EndaVideosPetitionsResources.fillVideoDataPostVideoUnregisteredUser().toString());
    }

    @Then("^the platform should display a message indicating \"([^\"]*)\"$")
    public void thePlatformShouldDisplayAMessageIndicating(String informationMessage) throws Throwable {
        PostVideoResponse.setResponse(then().extract().response()).validatePostVideoUnregisteredUser(informationMessage);
    }
}
