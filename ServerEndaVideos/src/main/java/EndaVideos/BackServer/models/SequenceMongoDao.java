package EndaVideos.BackServer.models;

import EndaVideos.BackServer.entities.Sequence;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SequenceMongoDao extends MongoRepository<Sequence, Integer> {

}
