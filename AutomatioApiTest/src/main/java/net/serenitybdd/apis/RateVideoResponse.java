package net.serenitybdd.apis;

import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;

public class RateVideoResponse {
    private Response responseContent;



    public RateVideoResponse(Response responseContent) {
        this.responseContent = responseContent;
    }

    public static RateVideoResponse setRateVideoResponse(Response responseContent){
        return new RateVideoResponse(responseContent);
    }

    public void validateRateFirstTime(Response videoInformation, String rate){
        int statusCodeResponse = responseContent.jsonPath().get("status");
        int idVideoRatedResponse = responseContent.jsonPath().get("id");
        int usefulCountVideoResponse = responseContent.jsonPath().get("useful");
        int notUsefulCountVideoResponse = responseContent.jsonPath().get("notUseful");

        int idVideoRated = videoInformation.jsonPath().get("id");
        int usefulCountVideo = videoInformation.jsonPath().get("useful");
        int notUsefulCountVideo = videoInformation.jsonPath().get("notUseful");

        Assert.assertThat("Correct status code was returned",statusCodeResponse, Matchers.equalTo(ResponseCodes.OK_STATUS));
        Assert.assertThat("Correct video rated identify by id",idVideoRatedResponse,Matchers.equalTo(idVideoRated));
        validateTypeOfRate(usefulCountVideoResponse, notUsefulCountVideoResponse, usefulCountVideo, notUsefulCountVideo, rate);

    }

    private void validateTypeOfRate(int usefulResponse, int notUsefulResponse, int useful, int notUseful, String rate){
        if(rate.equalsIgnoreCase("useful")){
            Assert.assertThat("The useful count of the video should increase",usefulResponse, Matchers.greaterThan(useful));
        }else {
            Assert.assertThat("The notUseful count of the video should increase",notUsefulResponse,Matchers.greaterThan(notUseful));
        }

    }

    public void validateRateAgainVideoSameCalification(Response videoInformation, String rate) {
        int statusCodeResponse = responseContent.jsonPath().get("status");
        int idVideoRatedResponse = responseContent.jsonPath().get("id");
        int usefulCountVideoResponse = responseContent.jsonPath().get("useful");
        int notUsefulCountVideoResponse = responseContent.jsonPath().get("notUseful");

        int idVideoRated = videoInformation.jsonPath().get("id");
        int usefulCountVideo = videoInformation.jsonPath().get("useful");
        int notUsefulCountVideo = videoInformation.jsonPath().get("notUseful");

        Assert.assertThat("Correct status code was returned",statusCodeResponse, Matchers.equalTo(ResponseCodes.OK_STATUS));
        Assert.assertThat("Correct video rated identify by id",idVideoRatedResponse,Matchers.equalTo(idVideoRated));
        validateRateDontChange(usefulCountVideoResponse, notUsefulCountVideoResponse, usefulCountVideo, notUsefulCountVideo, rate);

    }

    private void validateRateDontChange(int usefulResponse, int notUsefulResponse, int useful, int notUseful, String rate){
        if(rate.equalsIgnoreCase("useful")){
            Assert.assertThat("The useful count of the video should not increase",usefulResponse, Matchers.equalTo(useful));
        }else {
            Assert.assertThat("The notUseful count of the video should not increase",notUsefulResponse,Matchers.equalTo(notUseful));
        }
    }

    public void validateRateAgainVideoDifferentCalification(Response videoInformation, String rate) {
        int statusCodeResponse = responseContent.jsonPath().get("status");
        int idVideoRatedResponse = responseContent.jsonPath().get("id");
        int usefulCountVideoResponse = responseContent.jsonPath().get("useful");
        int notUsefulCountVideoResponse = responseContent.jsonPath().get("notUseful");

        int idVideoRated = videoInformation.jsonPath().get("id");
        int usefulCountVideo = videoInformation.jsonPath().get("useful");
        int notUsefulCountVideo = videoInformation.jsonPath().get("notUseful");

        Assert.assertThat("Correct status code was returned",statusCodeResponse, Matchers.equalTo(ResponseCodes.OK_STATUS));
        Assert.assertThat("Correct video rated identify by id",idVideoRatedResponse,Matchers.equalTo(idVideoRated));
        validateUpdateRateVideo(usefulCountVideoResponse, notUsefulCountVideoResponse, usefulCountVideo, notUsefulCountVideo, rate);
    }

    private void validateUpdateRateVideo (int usefulResponse, int notUsefulResponse, int useful, int notUseful, String rate){
        if(rate.equalsIgnoreCase("notUseful")){
            Assert.assertThat("The useful count of the video should decrease",usefulResponse, Matchers.lessThan(useful));
            Assert.assertThat("The notUseful count of the video should increase",notUsefulResponse,Matchers.greaterThan(notUseful));
        }else {
            Assert.assertThat("The notUseful count of the video should decrease",notUsefulResponse,Matchers.lessThan(notUseful));
            Assert.assertThat("The useful count of the video should increase",usefulResponse, Matchers.greaterThan(useful));
        }
    }
}
