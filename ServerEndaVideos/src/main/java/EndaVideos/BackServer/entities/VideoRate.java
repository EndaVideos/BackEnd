package EndaVideos.BackServer.entities;

public class VideoRate {

    private int id;

    private int idVideo;

    private int idUser;

    private boolean useful;

    public VideoRate(){}

    public VideoRate(int id, int idVideo, int idUser, boolean useful) {
        this.id = id;
        this.idVideo = idVideo;
        this.idUser = idUser;
        this.useful = useful;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(int idVideo) {
        this.idVideo = idVideo;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public boolean isUseful() {
        return useful;
    }

    public void setUseful(boolean useful) {
        this.useful = useful;
    }
}
