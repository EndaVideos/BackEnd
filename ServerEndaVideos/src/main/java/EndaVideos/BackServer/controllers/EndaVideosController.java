package EndaVideos.BackServer.controllers;

import EndaVideos.BackServer.dtos.UserDto;
import EndaVideos.BackServer.dtos.VideoDto;
import EndaVideos.BackServer.dtos.VideoRateDto;
import EndaVideos.BackServer.entities.User;
import EndaVideos.BackServer.entities.Video;
import EndaVideos.BackServer.models.SequenceMongoDao;
import EndaVideos.BackServer.models.UserDao;
import EndaVideos.BackServer.models.VideoMongoDao;
import EndaVideos.BackServer.models.VideoRateMongoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class EndaVideosController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SequenceMongoDao sequenceMongoDao;

    @Autowired
    private VideoMongoDao videoMongoDao;

    @Autowired
    private VideoRateMongoDao videoRateMongoDao;

    @PostMapping(value = "/user")
    public User createUser(@RequestBody UserDto userDto){
        return new UserController().createUser(userDto, userDao);
    }

    @PostMapping(value = "/video")
    public Video createVideo(@RequestBody VideoDto videoDto){
        return new VideoController(videoDto, videoMongoDao,sequenceMongoDao,userDao).createVideo();
    }

    @PostMapping(value = "/rate")
    public Video rateVideo(@RequestBody VideoRateDto videoRateDto){
        return new VideoRateController(videoRateMongoDao,videoMongoDao,videoRateDto, sequenceMongoDao).rateVideo();
    }

    @GetMapping(value = "/videos")
    public List<Video> listVideos(){
        return new VideoController(videoMongoDao).getFirstVideos();
    }

    @GetMapping(value = "/video")
    public List<Video> searchVideo(@RequestParam String videoToSearch){
        return new VideoController(videoMongoDao).getVideoConcidences(videoToSearch);
    }

    @GetMapping(value = "/video/{id}")
    public Video searchVideoById(@PathVariable Integer id){
        Video video = videoMongoDao.findById(id).orElseGet(()->{
            Video videoError = new Video();
            videoError.setMessage("Video not found");
            videoError.setStatus(404);
            return videoError;
        });
        video.setStatus(200);
        video.setMessage("Video found");
        return video;
    }


}
