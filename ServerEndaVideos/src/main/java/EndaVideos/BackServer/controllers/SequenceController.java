package EndaVideos.BackServer.controllers;

import EndaVideos.BackServer.entities.Sequence;
import EndaVideos.BackServer.models.SequenceMongoDao;

public class SequenceController {

    public int getNextSequence(SequenceMongoDao sequenceMongoDao){
        Sequence sequence = sequenceMongoDao.findById(1).orElse(null);
        int nextSequence = sequence.getSeq();
        sequence.setSeq(nextSequence+1);
        sequence.setId(1);
        sequenceMongoDao.save(sequence);
        return nextSequence;
    }

}
