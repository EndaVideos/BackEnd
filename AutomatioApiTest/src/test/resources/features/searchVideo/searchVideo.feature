Feature: Search a video
  As a user in the EndaVideos platform, I WANT search a video SO THAT i can selected for watch it.

  Scenario: Search for a video by a specific string  - Success
    Given the user searchs for the video "BDD for Dummies"
    When the user selects the search option
    Then A video list with coincidences for "BDD for Dummies" should be displayed

  Scenario: Search for a non-existing video - Success
    Given the user searchs for "fadfegabsefeadbdfsadfesed"
    When the user selects the search option
    Then an empty video list should be displayed