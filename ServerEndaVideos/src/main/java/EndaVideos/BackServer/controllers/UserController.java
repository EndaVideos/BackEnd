package EndaVideos.BackServer.controllers;

import EndaVideos.BackServer.dtos.UserDto;
import EndaVideos.BackServer.entities.User;
import EndaVideos.BackServer.models.UserDao;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserController {

    public User createUser(UserDto userDto, UserDao userDao){
        if(validateNick(userDto.getNick(), userDao)){
            User user = new User();
            user.setName(userDto.getName());
            user.setEmail(userDto.getEmail());
            user.setPassword(encryptPassword(userDto.getPassword()));
            user.setNick(userDto.getNick());
            userDao.save(user);
            return user;
        }
        return new User();
    }

    private boolean validateNick(String nick, UserDao userDao){
        User user = userDao.findByNick(nick);
        return user == null;
    }

    private String encryptPassword(String password) {
        String hashPassword = null;
        try {
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            sha256.update(password.getBytes("UTF-8"));
            byte[] digest = sha256.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte element : digest) {
                stringBuffer.append(String.format("%02x", element));
            }
            hashPassword = stringBuffer.toString();
        }
        catch (NoSuchAlgorithmException event) {
            System.out.println("Error: [" + event.getMessage() + "]");
        }
        catch (UnsupportedEncodingException event) {
            System.out.println("Error: [" + event.getMessage() + "]");
        }
        return hashPassword;
    }

}
