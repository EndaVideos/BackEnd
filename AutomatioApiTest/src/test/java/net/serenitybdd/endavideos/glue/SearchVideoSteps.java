package net.serenitybdd.endavideos.glue;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.apis.EndaVideosURLs;
import net.serenitybdd.apis.SearchVideoResponse;

import static net.serenitybdd.rest.SerenityRest.given;
import static net.serenitybdd.rest.SerenityRest.then;

public class SearchVideoSteps {
    RequestSpecification request;
    @Given("^the user searchs for the video \"([^\"]*)\"$")
    public void theUserSearchsForTheVideo(String videoToSearch) throws Throwable {
        request = given().contentType("application/json")
                .param("videoToSearch",videoToSearch);
    }

    @When("^the user selects the search option$")
    public void theUserSelectsTheSearchOption() throws Throwable {
        request.when().get(EndaVideosURLs.searchVideo());
    }

    @Then("^A video list with coincidences for \"([^\"]*)\" should be displayed$")
    public void aVideoListWithCoincidencesForShouldBeDisplayed(String videoToSearch) throws Throwable {
        SearchVideoResponse.setSearchVideoResponse(then().extract().response()).validateResultList(videoToSearch);
    }

    @Given("^the user searchs for \"([^\"]*)\"$")
    public void theUserSearchsFor(String videoToSearch) throws Throwable {
        request = given().contentType("application/json")
                .param("videoToSearch",videoToSearch);
    }

    @Then("^an empty video list should be displayed$")
    public void anEmptyVideoListShouldBeDisplayed() throws Throwable {
        SearchVideoResponse.setSearchVideoResponse(then().extract().response()).validateResultListIsEmpty();
    }
}
