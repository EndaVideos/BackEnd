package EndaVideos.BackServer.models;

import EndaVideos.BackServer.entities.VideoRate;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface VideoRateMongoDao extends MongoRepository<VideoRate, Integer> {
    VideoRate findByIdVideoAndIdUser(int idVideo, int idUser);
}
