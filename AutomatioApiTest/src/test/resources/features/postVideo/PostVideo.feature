Feature: Post a video
  As a user in the EndaVideos platform, I WANT TO post a video on my platform SO THAT other user can see it or search it.
  Scenario: Post a new video - Success
    Given the user wants to post a new video on his platform
    And the user loads the video with the necessary and valid information
    When the user select the post option
    Then the platform should displayed the new video with the sent information

  Scenario: Post a new video - without title  - Negative
    Given the user wants to post a new video on his platform
    And the user loads the video without the title
    When the user select the post option
    Then the platform display a message indicating "Video sent without a title"

  Scenario: Post a new video - unregistered user - Negative
    Given an user that is not registered in the platform
    When the user select the post option
    Then the platform should display a message indicating "User not found, maybe you are not register"
