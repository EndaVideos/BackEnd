package testResources;

import EndaVideos.BackServer.dtos.VideoDto;
import EndaVideos.BackServer.dtos.VideoRateDto;

public class TestResources {
    private static String userNick = "lzarate";
    private static String videoTitle = "Java fundamentals";
    private static String videoDescription = "Learn the basics concepts for develop in java";
    private static int idVideo = 1;
    private static int idUser = 1;

    public static VideoDto createVideoDto(){
        VideoDto videoDtoTest = new VideoDto();
        videoDtoTest.setTitle(videoTitle);
        videoDtoTest.setUserNick(userNick);
        videoDtoTest.setDescription(videoDescription);
        return videoDtoTest;
    }

    public static VideoRateDto createVideoRateDto(boolean rate){
        VideoRateDto videoRateDto = new VideoRateDto();
        videoRateDto.setIdVideo(idVideo);
        videoRateDto.setIdUser(idUser);
        videoRateDto.setUseful(rate);
        return videoRateDto;
    }
}
