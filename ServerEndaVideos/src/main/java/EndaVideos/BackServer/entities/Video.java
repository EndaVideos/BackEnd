package EndaVideos.BackServer.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Transient;
import java.util.Date;


public class Video {

    private int id;

    private String title;

    private String userNick;

    private int useful;

    private int notUseful;

    private String url;

    private Date publishDate;

    private String description;

    @JsonInclude
    @Transient
    private int status;

    @JsonInclude
    @Transient
    private String message;

    public Video(){}

    public Video(int id, String title, String userNick, int useful, int notUseful, String url, Date publishDate, String description, int status, String message) {
        this.id = id;
        this.title = title;
        this.userNick = userNick;
        this.useful = useful;
        this.notUseful = notUseful;
        this.url = url;
        this.publishDate = publishDate;
        this.description = description;
        this.status = status;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public int getUseful() {
        return useful;
    }

    public void setUseful(int useful) {
        this.useful = useful;
    }

    public int getNotUseful() {
        return notUseful;
    }

    public void setNotUseful(int notUseful) {
        this.notUseful = notUseful;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
