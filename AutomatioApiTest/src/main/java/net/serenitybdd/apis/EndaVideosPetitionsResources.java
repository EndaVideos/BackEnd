package net.serenitybdd.apis;

import com.google.gson.JsonObject;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.net.MalformedURLException;

import static net.serenitybdd.rest.SerenityRest.then;

public class EndaVideosPetitionsResources {
    private static JsonObject requestParams;
    private static int idVideoToRate = 1;

    public static JsonObject getRequestParams(){return requestParams;}

    public static JsonObject fillVideoDataPostVideo(){
        requestParams = new JsonObject();
        requestParams.addProperty("title","BDD For Dummies");
        requestParams.addProperty("userNick","lzarate");
        requestParams.addProperty("description","We jet have hope");
        return requestParams;
    }

    public static JsonObject fillVideoDataPostVideoWithOutTitle(){
        requestParams = new JsonObject();
        requestParams.addProperty("userNick","lzarate");
        return requestParams;
    }

    public static JsonObject fillVideoDataPostVideoUnregisteredUser(){
        requestParams = new JsonObject();
        requestParams.addProperty("title","Mi lucha");
        requestParams.addProperty("userNick", "dcastillo");
        return requestParams;
    }

    public static JsonObject fillRateVideoData(String useful){
        boolean rateUseful = false;
        requestParams = new JsonObject();
        requestParams.addProperty("idVideo",idVideoToRate);
        requestParams.addProperty("idUser", 1);
        if(useful.equalsIgnoreCase("useful")){
            rateUseful=true;
        }
        requestParams.addProperty("useful",rateUseful);
        return requestParams;
    }

    public static Response getVideoInformation(RequestSpecification requestSpecification) throws MalformedURLException {
        Response videoInformation;
        requestSpecification.when().get(EndaVideosURLs.getVideoById(idVideoToRate));
        videoInformation = then().extract().response();
        return videoInformation;
    }

    public static void setCalification(RequestSpecification requestSpecification, String useful) throws MalformedURLException {
        JsonObject rateVideo = fillRateVideoData(useful);
        requestSpecification.body(rateVideo.toString());
        requestSpecification.when().post(EndaVideosURLs.rateVideoUrl());
    }

    public static JsonObject fillRateVideoInverseRate(String useful){
        boolean rateUseful = true;
        requestParams = new JsonObject();
        requestParams.addProperty("idVideo",idVideoToRate);
        requestParams.addProperty("idUser", 1);
        if(useful.equalsIgnoreCase("useful")){
            rateUseful = false;
        }
        requestParams.addProperty("useful",rateUseful);
        return requestParams;
    }

    public static void setInverseCalification(RequestSpecification requestSpecification, String useful) throws MalformedURLException {
        JsonObject rateVideo = fillRateVideoInverseRate(useful);
        requestSpecification.body(rateVideo.toString());
        requestSpecification.when().post(EndaVideosURLs.rateVideoUrl());
    }

}
