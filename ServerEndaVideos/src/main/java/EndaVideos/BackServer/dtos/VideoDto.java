package EndaVideos.BackServer.dtos;

public class VideoDto {

    private int id;

    private String title;

    private String userNick;

    private String url;

    private String description;

    public VideoDto(){}

    public VideoDto(int id, String title, String userNick, String url, String description) {
        this.id = id;
        this.title = title;
        this.userNick = userNick;
        this.url = url;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
