package net.serenitybdd.apis;

public class ResponseCodes{
    public static int OK_STATUS = 200;
    public static int UNPROCESSABLE_ENTITY = 422;
    public static int NOT_FOUND = 404;


}
