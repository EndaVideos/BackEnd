package net.serenitybdd.endavideos.glue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.apis.EndaVideosURLs;
import net.serenitybdd.apis.EndaVideosPetitionsResources;
import net.serenitybdd.apis.RateVideoResponse;

import static net.serenitybdd.rest.SerenityRest.given;
import static net.serenitybdd.rest.SerenityRest.then;

public class RateVideoSteps {
    private RequestSpecification request;
    private Response videoInformation;
    @Given("^the user is watching a video$")
    public void theUserIsWatchingAVideo() throws Throwable {
        request = given().contentType("application/json");
    }


    @When("^the user selects one \"([^\"]*)\" for the video$")
    public void theUserSelectsOneForTheVideo(String useful) throws Throwable {
        videoInformation = EndaVideosPetitionsResources.getVideoInformation(request);
        request.body(EndaVideosPetitionsResources.fillRateVideoData(useful).toString());

    }

    @Then("^the \"([^\"]*)\" count of the rated video should increase$")
    public void theCountOfTheRatedVideoShouldIncrease(String useful) throws Throwable {
        request.when().post(EndaVideosURLs.rateVideoUrl());
        RateVideoResponse.setRateVideoResponse(then().extract().response()).validateRateFirstTime(videoInformation, useful);
    }

    @Given("^the user is watching again an already rated the video as \"([^\"]*)\"$")
    public void theUserIsWatchingAgainAnAlreadyRatedTheVideoAs(String useful) throws Throwable {
        request = given().contentType("application/json");
        EndaVideosPetitionsResources.setCalification(request, useful);
    }

    @When("^the user selects the same \"([^\"]*)\" for the video$")
    public void theUserSelectsTheSameForTheVideo(String useful) throws Throwable {
        videoInformation = EndaVideosPetitionsResources.getVideoInformation(request);
        request.body(EndaVideosPetitionsResources.fillRateVideoData(useful).toString());
        request.when().post(EndaVideosURLs.rateVideoUrl());

    }

    @Then("^the \"([^\"]*)\" count shouldn´t be updated$")
    public void theCountShouldnTBeUpdated(String useful) throws Throwable {
        RateVideoResponse.setRateVideoResponse(then().extract().response()).validateRateAgainVideoSameCalification(videoInformation, useful);
    }


    @Given("^the user is watching again an already rated video as \"([^\"]*)\"$")
    public void theUserIsWatchingAgainAnAlreadyRatedVideoAs(String useful) throws Throwable {
        request = given().contentType("application/json");
        EndaVideosPetitionsResources.setInverseCalification(request, useful);
    }

    @When("^the user changes his previusly \"([^\"]*)\"$")
    public void theUserChangesHisPreviusly(String useful) throws Throwable {
        videoInformation = EndaVideosPetitionsResources.getVideoInformation(request);
        request.body(EndaVideosPetitionsResources.fillRateVideoData(useful));
        request.when().post(EndaVideosURLs.rateVideoUrl());
    }

    @Then("^the \"([^\"]*)\" count should be updated$")
    public void theCountShouldBeUpdated(String useful) throws Throwable {
        RateVideoResponse.setRateVideoResponse(then().extract().response()).validateRateAgainVideoDifferentCalification(videoInformation, useful);
    }
}
