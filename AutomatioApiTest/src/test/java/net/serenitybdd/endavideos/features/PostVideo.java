package net.serenitybdd.endavideos.features;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/postVideo",
        glue = "net.serenitybdd.endavideos")
public class PostVideo {
}
