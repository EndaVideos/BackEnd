Feature: Rate a video
  As a user in the EndaVideos platform,I WANT to give a calification to the videos that i saw SO THAT i can indicate my opinion

  Scenario Outline: Rate for first time a video - Success
    Given the user is watching a video
    When the user selects one "<calification>" for the video
    Then the "<calification>" count of the rated video should increase
    Examples:
      | calification |
      | useful       |
      | notuseful    |

  Scenario Outline: Rate again a video with the same calification - Fail
    Given the user is watching again an already rated the video as "<calification>"
    When the user selects the same "<calification>" for the video
    Then the "<calification>" count shouldn´t be updated
    Examples:
      | calification |
      | useful       |
      | notUseful    |

  Scenario Outline: Rate again a video with a different calification - Success
    Given the user is watching again an already rated video as "<calification>"
    When the user changes his previusly "<calification>"
    Then the "<calification>" count should be updated
    Examples:
      | calification |
      | useful       |
      | notUseful       |




