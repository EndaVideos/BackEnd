package responseEntities;

public class VideoEntityResponse {

    private int id;
    private String title;
    private String userNick;
    private int useful;
    private int notUseful;
    private String url;

    public VideoEntityResponse(int id, String title, String userNick, int useful, int notUseful, String url) {
        this.id = id;
        this.title = title;
        this.userNick = userNick;
        this.useful = useful;
        this.notUseful = notUseful;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public int getUseful() {
        return useful;
    }

    public void setUseful(int useful) {
        this.useful = useful;
    }

    public int getNotUseful() {
        return notUseful;
    }

    public void setNotUseful(int notUseful) {
        this.notUseful = notUseful;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
