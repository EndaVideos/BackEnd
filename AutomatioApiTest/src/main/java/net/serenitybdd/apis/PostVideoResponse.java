package net.serenitybdd.apis;
import com.google.gson.JsonObject;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
public class PostVideoResponse {

    private Response responseContent;

    public PostVideoResponse(Response responseContent) {
        this.responseContent = responseContent;
    }

    public static PostVideoResponse setResponse(Response responseContent){
        return new PostVideoResponse(responseContent);
    }

    public void validatePostSuccessResponse(JsonObject informationSent){
        int statusResponse = responseContent.jsonPath().get("status");
        String titleVideoResponse = responseContent.jsonPath().get("title");
        String userVideoResponse = responseContent.jsonPath().get("userNick");
        Assert.assertThat("Correct status code was returned",statusResponse, Matchers.equalTo(ResponseCodes.OK_STATUS));
        Assert.assertThat("The title of the video is the same that was sent",titleVideoResponse,Matchers.equalToIgnoringCase(informationSent.get("title").getAsString()));
        Assert.assertThat("The user that post the video is the same that make the petition",userVideoResponse,Matchers.equalToIgnoringCase(informationSent.get("userNick").getAsString()));
    }

    public void validatePostVideoWithoutTitle(String informationMessage) {
        int statusResponse = responseContent.jsonPath().get("status");
        Assert.assertThat("Correct status code was returned",statusResponse, Matchers.equalTo(ResponseCodes.UNPROCESSABLE_ENTITY));
        validateResponseMessage(informationMessage);
    }

    public void validatePostVideoUnregisteredUser(String informationMessage) {
        int statusResponse = responseContent.jsonPath().get("status");
        Assert.assertThat("Correct status code was returned",statusResponse, Matchers.equalTo(ResponseCodes.NOT_FOUND));
        validateResponseMessage(informationMessage);
    }

    private void validateResponseMessage(String informationMessage){
        String responseMessage = responseContent.jsonPath().get("message");
        Assert.assertThat("Information message indicating the reason for the failure",responseMessage,Matchers.equalToIgnoringCase(informationMessage));
    }
}
