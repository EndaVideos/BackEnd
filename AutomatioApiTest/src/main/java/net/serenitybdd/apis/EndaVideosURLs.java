package net.serenitybdd.apis;


import java.net.MalformedURLException;
import java.net.URL;

import static java.lang.String.format;

public class EndaVideosURLs {

    private static String BASE_URL = "http://localhost:8080/";
    private static String POST_VIDEO = BASE_URL + "video";
    private static String RATE_VIDEO = BASE_URL + "rate";
    private static String GET_VIDEO_BY_ID = BASE_URL + "video";
    private static String SEARCH_VIDEO = BASE_URL + "video";

    public static URL postVideoUrl() throws MalformedURLException {
        return new URL(format(POST_VIDEO));
    }

    public static URL rateVideoUrl() throws MalformedURLException{
        return new URL(format(RATE_VIDEO));
    }

    public static URL getVideoById(int id)throws MalformedURLException{
        return new URL(format(GET_VIDEO_BY_ID +"/"+id));
    }

    public static URL searchVideo() throws MalformedURLException{
        return new URL(format(SEARCH_VIDEO));
    }



}
