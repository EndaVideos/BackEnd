package EndaVideos.BackServer.models;

import EndaVideos.BackServer.entities.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserDao extends CrudRepository<User, Integer> {
    User findByNick(String nick);
}
