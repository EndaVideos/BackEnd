package EndaVideos.BackServer.entities;

public class Sequence {

    private int seq;

    private int id;

    public Sequence(){}

    public Sequence(int seq, int id) {
        this.seq = seq;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

}
