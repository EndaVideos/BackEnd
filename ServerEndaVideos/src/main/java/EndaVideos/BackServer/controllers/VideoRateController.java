package EndaVideos.BackServer.controllers;

import EndaVideos.BackServer.dtos.VideoRateDto;
import EndaVideos.BackServer.entities.Video;
import EndaVideos.BackServer.entities.VideoRate;
import EndaVideos.BackServer.models.SequenceMongoDao;
import EndaVideos.BackServer.models.VideoMongoDao;
import EndaVideos.BackServer.models.VideoRateMongoDao;

public class VideoRateController {

    private VideoRateMongoDao videoRateMongoDao;
    private VideoMongoDao videoMongoDao;
    private VideoRateDto videoRateDto;
    private SequenceMongoDao sequenceMongoDao;

    public VideoRateController(VideoRateMongoDao videoRateMongoDao, VideoMongoDao videoMongoDao, VideoRateDto videoRateDto, SequenceMongoDao sequenceMongoDao) {
        this.videoRateMongoDao = videoRateMongoDao;
        this.videoMongoDao = videoMongoDao;
        this.videoRateDto = videoRateDto;
        this.sequenceMongoDao = sequenceMongoDao;
    }


    public Video rateVideo(){
        VideoRate videoRate = validateVideoRate();
        if(videoRate==null){
            videoRate = createNewRate();
            return addRateToTheVideo(videoRate);
        }
        return updateRateToTheVideo(videoRate);
    }

    private VideoRate createNewRate() {
        if(validateVideo()){
            VideoRate videoRate = new VideoRate();
            videoRate.setId(new SequenceController().getNextSequence(sequenceMongoDao));
            videoRate.setIdVideo(videoRateDto.getIdVideo());
            videoRate.setIdUser(videoRateDto.getIdUser());
            videoRate.setUseful(videoRateDto.isUseful());
            return videoRate;
        }
        return new VideoRate();
    }

    private VideoRate validateVideoRate(){
        return videoRateMongoDao.findByIdVideoAndIdUser(videoRateDto.getIdVideo(), videoRateDto.getIdUser());
    }

    private  boolean validateVideo (){
        Video video = videoMongoDao.findById(videoRateDto.getIdVideo()).orElse(null);
        return video != null;
    }

    private Video updateRateToTheVideo (VideoRate videoRate){
        Video videoToRate = videoMongoDao.findById(videoRate.getIdVideo()).orElse(null);
        if(videoToRate!=null){
            videoToRate.setId(videoRate.getIdVideo());
            if(videoRate.isUseful()!=videoRateDto.isUseful()){
                videoRate.setId(videoRate.getId());
                videoRate.setUseful(videoRateDto.isUseful());
                videoRateMongoDao.save(videoRate);
                updateVideoRateCounts(videoToRate);
                videoMongoDao.save(videoToRate);
                videoToRate.setStatus(200);
                videoToRate.setMessage("Video Rated");
                return videoToRate;
            }
            videoToRate.setStatus(200);
            videoToRate.setMessage("You already rate this video with this calification");
            return videoToRate;
        }
        videoToRate = new Video();
        videoToRate.setStatus(404);
        videoToRate.setMessage("The video that you are trying to rate not exist");
        return videoToRate;
    }

    private Video addRateToTheVideo(VideoRate videoRate){
        Video videoToRate = videoMongoDao.findById(videoRate.getIdVideo()).orElse(null);
        if(videoToRate!=null){
            videoToRate.setId(videoRate.getIdVideo());
            addNewRateToVideoCounts(videoToRate);
            videoRateMongoDao.save(videoRate);
            videoMongoDao.save(videoToRate);
            videoToRate.setStatus(200);
            videoToRate.setMessage("Video Rated");
            return videoToRate;
        }
        videoToRate = new Video();
        videoToRate.setStatus(404);
        videoToRate.setMessage("The video that you are trying to rate not exist");
        return videoToRate;
    }

    private void updateVideoRateCounts(Video videoToUpdate){
        if(videoRateDto.isUseful()){
            videoToUpdate.setUseful(videoToUpdate.getUseful()+1);
            videoToUpdate.setNotUseful(videoToUpdate.getNotUseful()-1);
        }else {
            videoToUpdate.setUseful(videoToUpdate.getUseful()-1);
            videoToUpdate.setNotUseful(videoToUpdate.getNotUseful()+1);
        }
    }

    private void addNewRateToVideoCounts(Video videoToAdd){
        if(videoRateDto.isUseful()){
            videoToAdd.setUseful(videoToAdd.getUseful()+1);
        }else {
            videoToAdd.setNotUseful(videoToAdd.getNotUseful()+1);
        }
    }


}
