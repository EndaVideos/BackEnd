package EndaVideos.BackServer;

import EndaVideos.BackServer.controllers.VideoController;
import EndaVideos.BackServer.controllers.VideoRateController;
import EndaVideos.BackServer.dtos.VideoDto;
import EndaVideos.BackServer.dtos.VideoRateDto;
import EndaVideos.BackServer.endaVideosProperties.VideoProperties;
import EndaVideos.BackServer.entities.Video;
import EndaVideos.BackServer.entities.VideoRate;
import EndaVideos.BackServer.models.SequenceMongoDao;
import EndaVideos.BackServer.models.UserDao;
import EndaVideos.BackServer.models.VideoMongoDao;
import EndaVideos.BackServer.models.VideoRateMongoDao;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import testResources.TestResources;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BackServerApplicationTests {

	@Autowired
	private VideoMongoDao videoMongoDao;

	@Autowired
	private SequenceMongoDao sequenceMongoDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private VideoRateMongoDao videoRateMongoDao;

	@Test
	public void createVideo() {
		VideoDto videoDtoTest = TestResources.createVideoDto();
		Video testVideoCreated = new VideoController(videoDtoTest, videoMongoDao, sequenceMongoDao, userDao).createVideo();
		Assert.assertThat("The video has the sent title", testVideoCreated.getTitle(), Matchers.equalTo(videoDtoTest.getTitle()));
		Assert.assertThat("The video has the sent description",testVideoCreated.getDescription(),Matchers.equalTo(videoDtoTest.getDescription()));
		Assert.assertThat("The useful rate count of the video was assigned",testVideoCreated.getUseful(),Matchers.equalTo(0));
		Assert.assertThat("The not useful rate count of the video was assigned",testVideoCreated.getNotUseful(),Matchers.equalTo(0));
	}

	@Test
	public void getVideoConcidences(){
		String videoToSearch = "BDD for Dummies";
		List<Video> videosResultListTest = new VideoController(videoMongoDao).getVideoConcidences(videoToSearch);
		Assert.assertThat("The first result of the videos list should coincide with the search video",videosResultListTest.get(0).getTitle(),Matchers.equalToIgnoringCase(videoToSearch));
	}

	@Test
	public void getFirstVideos(){
		List<Video> videosResultListTest = new VideoController(videoMongoDao).getFirstVideos();
		Assert.assertThat("The videos in the result list should has a popularity of:",videosResultListTest.get(0).getUseful(),
				Matchers.anyOf(Matchers.equalTo(VideoProperties.POPULARITY),Matchers.greaterThan(VideoProperties.POPULARITY)));
	}

	@Test
	public void createRateUseful(){
		VideoRateDto videoRateUseful = TestResources.createVideoRateDto(true);
		Video videoRatedTest = new VideoRateController(videoRateMongoDao,videoMongoDao,videoRateUseful,sequenceMongoDao).rateVideo();
		Assert.assertThat("The rate has the sent calification",videoRatedTest.getUseful(),Matchers.greaterThan(0));
	}

	@Test
	public void createRateNotUseful(){
		VideoRateDto videoRateDtoNotUseful = TestResources.createVideoRateDto(false);
		Video videoRatedTest = new VideoRateController(videoRateMongoDao,videoMongoDao,videoRateDtoNotUseful,sequenceMongoDao).rateVideo();
		Assert.assertThat("The rate has the sent calification",videoRatedTest.getNotUseful(),Matchers.greaterThan(0));
	}

}
