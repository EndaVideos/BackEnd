package net.serenitybdd.apis;


import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;

import java.util.ArrayList;

public class SearchVideoResponse {
    private Response responseContent;

    public SearchVideoResponse(Response responseContent) {
        this.responseContent = responseContent;
    }

    public static SearchVideoResponse setSearchVideoResponse(Response responseContent){
        return new SearchVideoResponse(responseContent);
    }

    public void validateResultList(String videoToSearch){
        int statusCodeResponse = responseContent.getStatusCode();
        String titleVideoResponse = responseContent.jsonPath().get("title[0]");
        ArrayList videoResulList = responseContent.jsonPath().get();
        Assert.assertThat("Correct status code was returned",statusCodeResponse, Matchers.equalTo(ResponseCodes.OK_STATUS));
        Assert.assertThat("The list of videos has at least one result",videoResulList.size(),Matchers.greaterThan(0));
        Assert.assertThat("The list of videos has coincidences with the video searched",titleVideoResponse,Matchers.equalToIgnoringCase(videoToSearch));

    }


    public void validateResultListIsEmpty() {
        int statusCodeResponse = responseContent.getStatusCode();
        ArrayList videoResulList = responseContent.jsonPath().get();
        Assert.assertThat("Correct status code was returned",statusCodeResponse, Matchers.equalTo(ResponseCodes.OK_STATUS));
        Assert.assertThat("The list of videos are empty",videoResulList.size(),Matchers.equalTo(0));
    }
}
